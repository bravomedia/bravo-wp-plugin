<?php

/**
 * The plugin bootstrap file
 *
 * @link              http://www.bravomedia.se/
 * @since             1.0.0
 * @package           Bravo_WP_Plugin
 *
 * @wordpress-plugin
 * Plugin Name:       Bravo_WP_Plugin
 * Plugin URI:        http://www.bravomedia.se/
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Bravomedia
 * Author URI:        http://www.bravomedia.se/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       bravo-wp-plugin
 */

if(!defined('WPINC')) {
    die;
}

class Bravo_WP_Plugin {
    public $dir;
    public $name;
    public $public_init_file;
    public $admin_init_file;
    public static $instance;

    static function init() {
        static::$instance = new static;
    }

    function __construct(array $args = array()) {
        $this->dir = isset($args['dir']) ? $args['dir'] : __DIR__;
        $this->name = isset($args['name']) ? $args['name'] : pathinfo(__DIR__, PATHINFO_BASENAME);
        $this->public_init_file = $this->dir . '/public/public.php';
        $this->admin_init_file = $this->dir . '/admin/admin.php';
        $this->include_dependencies();
        register_activation_hook(__FILE__, array($this, 'activate'));
        register_deactivation_hook(__FILE__, array($this, 'deactivate'));
        $this->init_public();
        $this->init_admin();
    }

    function file_url($file, $version = true) {
        $result = plugin_dir_url($this->dir . '/' . $file);
        $result = $result . basename($file);
        if($version) {
            $result .= '?' . $this->file_version($file);
        }
        return $result;
    }

    function file_path($file) {
        $result = plugin_dir_path($this->dir . '/' . $file);
        $result = $result . basename($file);
        return $result;
    }

    function file_version($file) {
        $result = $this->file_path($file);
        return substr(dechex(filemtime($result)), -4);
    }

    function include_dependencies() {
        // Include any dependencies here
        //require_once __DIR__ . '/include/MyClass.php';
    }

    function activate() {
        // Activation code goes here
    }

    function deactivate() {
        // Deactivation code goes here
    }

    function init_public() {
        if(is_file($this->public_init_file)) {
            $plugin = $this;
            require_once $this->public_init_file;
        }
    }

    function init_admin() {
        if(is_admin() && is_file($this->admin_init_file)) {
            $plugin = $this;
            require_once $this->admin_init_file;
        }
    }

    function option_key($key) {
        return $this->name . '-' . $key;
    }

    function get_option($key, $default) {
        return get_option($this->option_key($key), $default);
    }

    function set_option($key, $value, $autoload = null) {
        update_option($this->option_key($key), $value, $autoload);
    }
}

Bravo_WP_Plugin::init();
