<?php

if(!defined('WPINC')) {
    die;
}

$plugin_menu = new stdClass;

$plugin_menu->index = function() use($plugin) {
    require __DIR__ . '/views/index.php';
};

//$plugin_menu->submenu = function() use($plugin) {
//    require __DIR__ . '/views/submenu.php';
//};

add_action('admin_menu', function() use($plugin, $plugin_menu) {
    $prefix = 'plugin-menu';
    add_menu_page('Example menu', 'Example menu', 'manage_options', $prefix, $plugin_menu->index);
    //add_submenu_page($prefix, 'Submenu-item', 'Submenu-item', 'manage_options', $prefix . '-submenu', $plugin_menu->submenu);
});

add_action('admin_enqueue_scripts', function() use($plugin) {
    wp_enqueue_style($plugin->name, $plugin->file_url('admin/build/admin.css', true), array(), false, 'all');
    wp_enqueue_script($plugin->name, $plugin->file_url('admin/build/admin.js', true), array('jquery'), false, false);
});
